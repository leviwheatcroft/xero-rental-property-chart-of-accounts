## Xero Rental Property Chart of Accounts

This chart of accounts is a bare bones chart including income and expense
accounts as they appear in your Australian Income Tax Return, and the basic xero
system accounts.

## How to Use This
Download the csv [here](https://gitlab.com/leviwheatcroft/xero-rental-property-chart-of-accounts/raw/master/ChartOfAccounts.csv?inline=false)
upload it when setting up the xero subscription, or import it to an existing subscription.

## Contributing

PRs welcome

## Questions / Feedback

Open an issue on this repository.
